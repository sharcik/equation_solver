require 'spec_helper'
require 'rails_helper'



RSpec.describe "Login page", type: :request do

  before(:each) do
    visit login_path
    fill_in :user_username, with: 'stas@mail.ru'
    fill_in :user_password, with: '123456'
  end

  subject {page}

  it { should have_content('Login') }

  describe "wrong login" do

    context "with empty fields" do
      before do
        fill_in :user_username, with: ""
        fill_in :user_password, with: "" 
        click_button    
      end
      it { should have_content("You don't authorized!") }
      it { should have_selector('div.alert-info') }
    end

    context "with password less then 6 sumbols" do
      before do
        fill_in :user_username, with: "stas@mail.ru"
        fill_in :user_password, with: "123" 
        click_button    
      end
      it { should have_content("You don't authorized!") }
      it { should have_selector('div.alert-info') }
    end

    context "with not correct email format" do
      before do
        fill_in :user_username, with: "stasmail.ru"
        fill_in :user_password, with: "123456" 
        click_button    
      end
      it { should have_content("You don't authorized!") }
      it { should have_selector('div.alert-info') }
    end

  end


  describe "correct login" do
    before do
      click_button 
    end

    it { should have_content("System for solving mathematical equations") }
    it { should have_content("You log in!") }
    it { should have_selector('div.alert-info') }
  end


end