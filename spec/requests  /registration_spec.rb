require 'spec_helper'
require 'rails_helper'

RSpec.describe "Registration page", type: :request do

  before(:each) do
    visit registration_path
    fill_in :user_username, with: 'zxcvbn@mail.ru'
    fill_in :user_password, with: '123456'
    fill_in :user_password_confirmation, with: '123456'
  end

  subject {page}

  it { should have_content('Registration') }

  describe "wrong registration data" do

    context "with empty fields" do
      before do
        fill_in :user_username, with: ""
        fill_in :user_password, with: ""
        fill_in :user_password_confirmation, with: ""
        click_button    
      end
      it { should have_content("You don't authorized!") }
      it { should have_selector('div.alert-info') }
    end

    context "fields password and password_confirmation must match" do
      before do
        fill_in :user_password_confirmation, with: "123"
        click_button    
      end
      it { should have_content("You don't authorized!") }
      it { should have_selector('div.alert-info') }
    end

    context "fields can't be less then 6 symbols" do
      before do
      	fill_in :user_password, with: "123"
        fill_in :user_password_confirmation, with: "123"
        click_button    
      end
      it { should have_content("You don't authorized!") }
      it { should have_selector('div.alert-info') }
    end

    context "with not correct email format" do
      before do
        fill_in :user_username, with: "examplemail.ru"
        click_button    
      end
      it { should have_content("You don't authorized!") }
      it { should have_selector('div.alert-info') }
    end

  end


  describe "correct login" do
    before do
      click_button    
    end

    it { should have_content("System for solving mathematical equations") }
    it { should have_content("Successful registration") }
    it { should have_selector('div.alert-info') }
    
  end


end