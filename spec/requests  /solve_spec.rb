require 'spec_helper'
require 'rails_helper'


RSpec.describe "Solve API", type: :request do

  before(:each) do
    visit login_path
    fill_in :user_username, with: 'stas@mail.ru'
    fill_in :user_password, with: '123456'
    click_button  
  end

  subject {page}

  it { should have_content('System for solving mathematical equations') }

  describe "wrong data", js: true do

    context "with empty field" do
      before do
        fill_in :equation_equation_text, with: ""
        click_button    
      end
      it { should have_content("Equation text can't be blank and Equation text is too short (minimum is 3 characters)") }
      it { should have_selector('div.alert-info') }
    end

    context "with less then 3 sumbols" do
      before do
        fill_in :equation_equation_text, with: "12"
        click_button    
      end
      it { should have_content("Equation text is too short (minimum is 3 characters)") }
      it { should have_selector('div.alert-info') }
    end

    context "with more then 40 sumbols" do
      before do
        fill_in :equation_equation_text, with: "#{'4'*41}"
        click_button    
      end
      it { should have_content("Equation text is too long (maximum is 40 characters)") }
      it { should have_selector('div.alert-info') }
    end


  end


  describe "correct equation", js: true do
    context "linear without spaces" do
      before do
        fill_in :equation_equation_text, with: "x+5x=12"
        click_button    
      end
      it { should have_content("X = 2.0") }
      it { should have_content("Answer:") }
    end

    context "linear with spaces" do
      before do
        fill_in :equation_equation_text, with: "x + 5x = 12"
        click_button    
      end
      it { should have_content("X = 2.0") }
      it { should have_content("Answer:") }
    end
  

    context "quadratic without spaces" do
      before do
      	select "quadratic", :from => "equation_type"
        fill_in :equation_equation_text, with: "x^2+3x-3=-x^2-2x"
        click_button    
      end
      it { should have_content("Roots: 0.5, -3.0") }
      it { should have_content("Answer:") }
    end

    context "quadratic with spaces" do
      before do
      	select "quadratic", :from => "equation_type"
        fill_in :equation_equation_text, with: "x^2+3x- 3 =-x^2- 2x"
        click_button    
      end
      it { should have_content("Roots: 0.5, -3.0") }
      it { should have_content("Answer:") }
    end

  end
end