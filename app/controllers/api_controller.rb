class ApiController < ApplicationController

  def login
    username = params[:user][:username]
    password = params[:user][:password]
    
    response = send_api('login',{ username: username, password: password}.to_json)

    check_response(response)
    redirect_after_authorization(response)
  end
    
  def registration
    username = params[:user][:username]
    password = params[:user][:password]
    password_confirmation = params[:user][:password_confirmation]
    
    response = send_api('registration',{ username: username, password: password, password_confirmation: password_confirmation}.to_json)
    
    check_response(response)
    redirect_after_authorization(response)
  end
    
    
  def solve
    response = send_api('solve',params[:equation].to_json)
    check_response(response)
    data = response.body
    
    if data['errors']
      @answer = "Not the correct format of the equation"
      flash.now[:notice] = JSON.parse(data)['errors']
    else
      @answer = data 
    end
  end
  
  
  private
  
  
  def send_api(url,data)
    call_api = SendApi.new(url,data,cookies[:token])
    call_api.send    
  end
  
  def check_response(response)
    case response.code
    when 200
      puts "Log: The request was successful"
    when 401
      raise Errors::NotAuthorized
    else
      raise Errors::ApiError
    end    
  end

  def redirect_after_authorization(response)
    data = JSON.parse(response.body)
    flash[:notice]  = data['message']
    cookies[:token] = data['token']
    redirect_to root_path 
  end
    
end
