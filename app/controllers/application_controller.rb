class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  rescue_from Errors::NotAuthorized, :with => :user_not_authorized
  rescue_from Errors::ApiError, :with => :api_error

  private

  def user_not_authorized
    flash[:error] = "You don't authorized!"
    redirect_to :back
  end

  def api_error
    flash[:error] = "Server error!"
    redirect_to :back
  end
 
end
