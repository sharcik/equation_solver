require 'rest-client'


class SendApi

  def initialize(url, data, token)
    @url   = "http://localhost:4567/#{url}"
    @data  = data
    @token = token 
  end

  def send
    begin
      RestClient.post(@url, @data, {AUTHORIZATION: @token})
    rescue => e
      e.response
    end
  end


end

