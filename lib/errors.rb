module Errors
  class NotAuthorized < StandardError; end
  class ApiError < StandardError; end
end
